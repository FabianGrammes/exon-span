import HTSeq, collections, optparse, sys

#---------------------------------------------------------------------
# ARGUMENTS

optParser = optparse.OptionParser(
    usage = "python %prog [options] <flattened_gff_file> <alignment_file> <output_file>")

optParser.add_option( "-f", "--format", type="choice", dest="alignment",
   choices=("sam", "bam"), default="sam",
   help = "'sam' or 'bam'. Format of <alignment file> (default: sam)" )

optParser.add_option( "-s", "--stranded", type="choice", dest="stranded",
   choices = ( "fs", "ss", "non" ), default = "non",
   help = "Library type used; specify only when a PE library is used."+
   "'fs' = firststrand: r2--> <--1; dUTP, NSR, NNSR (Illumina)"+
   "'ss' = secondstrand: r1--> <--r2; Liagtion, Standard SOLiD."+
   "default = 'non': non stranded" )

optParser.add_option( "-p", "--pairedend", type="choice", dest="pe",
   choices = ( "yes", "no" ), default = "no",
   help = "When using the 'yes' option: Only properly paired reads "+
   "(SAM flag 0x0002) and primary reads (SAM flag: 0x0100) will be "+
   "counted, reads that are not properly paired or not unique "+
   "will be skipped. IPORTANT bam/sam file MUST be sorted by name!!! "+
   "default: no" )

optParser.add_option( "-r", "--report", type="choice", dest="rep",
   choices = ( "full", "chop" ), default = "chop",
   help = "Report format: 'full' = Ex:1-Ex:2-Ex3 "+
   "'chop' = Ex:1-Ex:2; Ex:2-Ex:3" )

optParser.add_option( "-m", "--mapq", type="int", dest="mq",
    default = -1 ,
    help = "Mapping quality filter: Reads with MAPQ < value will be ignored. "+
    "Unique hits have MAPQ = 255, MAPQ = -10log10 Pr(Mapping pos wrong). "+
    "Default = 0 (no filtering)")

optParser.add_option( "-o", "--overlap", type="int", dest="overlap",
    default = 8,
    help = "The minimum bases that a read has to overlap with an exonic part in"+ 
    "order to counted")


#---------------------------------------------------------------------
# FUNCTIONS

# function to read in gff file
def feature_reader( gff_file, stranded ):
    features = HTSeq.GenomicArrayOfSets( "auto", stranded=stranded )     
    for f in  HTSeq.GFF_Reader( gff_file ):
        if f.type != "exonic_part":
            continue
        else:
            f.name = f.attr['gene_id'] + ":" + f.attr['exonic_part_number']
        features[f.iv] += f
    return(features)

# function to parse the hits from the 'identMs' function to a readable
# output
def update_count_vec( rs ):
    cig_out = []
    if (type(rs) == str):
        out = rs
    else:
        for f in rs:
            if ':' in f.name:
                cig_out.append( f.name)
        out = '-'.join(sorted(cig_out))
    return out

# same function as above modified for usage with PE data
def update_count_vec_PE( fr, sr ):
    cig_out = []
    if (type(fr) == str):
        cig_out.append( fr )
    else:
        for f in fr:
            if ':' in f.name:
                cig_out.append( f.name)
    if (type(sr) == str):
        cig_out.append( sr )
    else:
        for s in sr:
            if ':' in s.name:
                cig_out.append( s.name)
    out = '-'.join(sorted(set(cig_out)))
    return out

# Function returns an index of the M's in the cigar list
def findMs( almnt ):
    lcig = [ct.type for ct in almnt.cigar] 
    # indN = [i for i, s in enumerate(lcig) if 'N' == s]
    indM = [i for i, s in enumerate(lcig) if 'M' == s]
    return [indM]

def identMs( almnt, mlist, strand_fun, features, overlap):
    rs = set()
    c_counter = -1
    for cig in almnt.cigar:
        c_counter += 1
        if cig.type in ['S','I','D','H','P','X']:
            continue
        cmatch = [True for f in mlist if (f[0] == c_counter) or (f[1] == c_counter)]
        if len(cmatch) > 0:
            for iv, s in features[ strand_fun(cig.ref_iv) ].steps():
                # only accept patches if they are overlapping at least x bases
                if iv.length > overlap:
                    rs = rs.union(s)
                    
    set_of_exon_names = set( [ f.name.split(":")[0] for f in rs ] )
    if len( set_of_exon_names ) == 0:
        return '_empty'
    elif len( set_of_exon_names ) > 1:
        return '_ambigious'
    else:
        return rs

# function to polish the output
# splits reads spanning multiple exons like 'XLOC_105064:006-XLOC_105064:009-XLOC_105064:008'
# into pairs of 2
def polish_out(ex_list):
    polished_results = collections.Counter( )
    for eid in ex_list:
        if len( eid.split('-') ) > 2:
            splitted = eid.split('-')
            for i in range(0, len(splitted)-1 ):
                new_eid = '-'.join(splitted[i:i+2])
                polished_results[new_eid] += ex_list[eid]
        elif len( eid.split('-') ) == 2:
            polished_results[eid] += ex_list[eid]
        elif len(eid.split('-') ) == 1:
            polished_results['_ambigous'] += 1
        elif eid == '':
            pass
    return polished_results

# function to ensure strand conformity
def invert_strand( iv ):
    iv2 = iv.copy()
    if iv2.strand == "+":
        iv2.strand = "-"
    elif iv2.strand == "-":
        iv2.strand = "+"
    else:
        raise ValueError, "Illegal strand"
    return iv2

def same_strand( iv ):
    return iv
#---------------------------------------------------------------------
# ARGUMENTS

(opts, args) = optParser.parse_args()

# read options
gff_file = args[0]
sam_file = args[1]
out_file = args[2]
alignment = opts.alignment
lyb_type = opts.stranded
PE = opts.pe
report = opts.rep
mapq = opts.mq
overlap = opts.overlap

# gff_file = "example/TYSND1.gff"
# sam_file = "example/192-S1-D0-HK-HR-2_TYSND1.bam"
# out_file = "tysnd_OUT.txt"
# alignment = "bam"
# lyb_type = "fs"
# PE = "yes"
# report = "chop"
# mapq = 255

# allow pipe
if sam_file == "-":
   sam_file = sys.stdin

# read sam/bam
if alignment == "sam":
   file_reader = HTSeq.SAM_Reader
else:
   file_reader = HTSeq.BAM_Reader

# library preparation first second or non...
if lyb_type == 'fs':
    stranded = True
    first_fun = invert_strand
    second_fun = same_strand
elif lyb_type == 'ss':
    stranded = True
    first_fun = same_strand
    second_fun = invert_strand
elif lyb_type == 'non':
    stranded = False
    first_fun = same_strand
    second_fun =same_strand


#---------------------------------------------------------------------
# SCRIPT

# read the gff
features = feature_reader( gff_file, stranded )

# results object
results = collections.Counter( )


# NOT paired-end reads
#............................
if PE == 'no':

    for almnt in file_reader( sam_file ):
        if almnt.aQual <= mapq:
            continue
        lcig = [ct.type for ct in almnt.cigar]
        if 'N' not in lcig:
            continue
        else:
            mlist = findMs( almnt )
            out1 = identMs( almnt, mlist, first_fun, features)
            spann_id = update_count_vec( out1 )
            results[ spann_id ] += 1

    # polish results
    if report == 'chop':
        # polish results
        results = polish_out(results)

    # write to file
    outfile = open(out_file, 'w')
    for eid in sorted(results):
        if eid == '':
            pass
        else:
            outfile.write( '%s\t%s\n' % (eid, results[ eid ]))
    outfile.close()
#...........................


# PE
#..........................
if PE == 'yes':

    almnt_file = file_reader( sam_file )
    for first, second in HTSeq.pair_SAM_alignments_with_buffer( almnt_file, max_buffer_size=3000000):
        if (first is None) or (second is None):
            continue
        if (first.aQual < mapq) or (second.aQual < mapq):
            continue
        fr_cig = [ct.type for ct in first.cigar]
        sr_cig = [ct.type for ct in second.cigar]
        mlist = findMs( first )
        out_fr = identMs( first, mlist, first_fun, features, overlap)
        mlist = findMs( second )
        out_sr = identMs( second, mlist, second_fun, features, overlap)
        if (out_fr == '_empty') or (out_sr == '_empty'):
            spann_id = '_empty'
        else:
            spann_id = update_count_vec_PE( out_fr, out_sr)
        results[ spann_id ] += 1

    if report == 'chop':
            # polish results
        results = polish_out(results)

    # write to file
    outfile = open(out_file, 'w')
    for eid in sorted(results):
        if eid == '':
            pass
        else:
            outfile.write( '%s\t%s\n' % (eid, results[ eid ]))
    outfile.close()



